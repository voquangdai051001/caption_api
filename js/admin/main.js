import { batLoading, tatLoading } from "../master.js";
import {
  showThongTinLenForm,
  renderDS,
  layThongTinTuForm,
} from "./controller.js";

const BAST_URL = "https://6404131480d9c5c7bac044c0.mockapi.io";

// Lấy Thông tin
export let featchPhone = () => {
  batLoading();
  axios({
    url: `${BAST_URL}/phone`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      let dsPhone = res.data;
      renderDS(dsPhone);
    })
    .catch((err) => {
      console.log(err);
    });
};
featchPhone();

// Thêm
let addPhone = () => {
  let phone = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BAST_URL}/phone`,
    method: "POST",
    data: phone,
  })
    .then((res) => {
      featchPhone();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
document.getElementById("addPhone").addEventListener("click", addPhone);

// Xóa
let deletePhone = (id) => {
  batLoading();
  axios({
    url: `${BAST_URL}/phone/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      featchPhone();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deletePhone = deletePhone;

// Sửa
let editPhone = (id) => {
  batLoading();
  axios({
    url: `${BAST_URL}/phone/${id}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      showThongTinLenForm(res.data);
      console.log(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.editPhone = editPhone;

// Cập nhật
let update = () => {
  batLoading();
  let phone = layThongTinTuForm();
  console.log("phone: ", phone);

  axios({
    url: `${BAST_URL}/phone/${phone.id}`,
    method: "PUT",
    data: phone,
  })
    .then((res) => {
      resetForm();
      featchPhone();
      console.log(res.data.img);
    })
    .catch((err) => {
      console.log(err);
    });
};
document.getElementById("update").addEventListener("click", update);

let resetForm = () => {
  document.getElementById("formDSPhone").reset();
};
document.getElementById("resetForm").addEventListener("click", resetForm);
