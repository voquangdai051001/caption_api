export let renderDS = (dsPhone) => {
  let contentHTML = "";
  for (let i = 0; i < dsPhone.length; i++) {
    let contenTr = `<tr>
            <td>${dsPhone[i].id}</td>
            <td>${dsPhone[i].name}</td>
            <td>${dsPhone[i].price}</td>
            <td>${dsPhone[i].screen}</td>
            <td>${dsPhone[i].backCamera}</td>
            <td>${dsPhone[i].fontCamera}</td>
            <td>
            <img style='width:200px' src="${dsPhone[i].img}" alt="">
            </td>
            <td>${dsPhone[i].desc}</td>
            <td>${dsPhone[i].type}</td>
            <td>
            <button onclick='editPhone(${dsPhone[i].id})' class='btn btn-warning'>Edit</button>
            <button onclick='deletePhone(${dsPhone[i].id})' class='btn btn-danger'>Delete</button>
            </td>
        </tr>`;
    contentHTML += contenTr;
  }
  document.getElementById("tbodyPhone").innerHTML = contentHTML;
};

export let showThongTinLenForm = (phone) => {
  document.getElementById("txtIdPhone").value = phone.id;
  document.getElementById("txtNamePhone").value = phone.name;
  document.getElementById("txtPrice").value = phone.price;
  document.getElementById("txtScreen").value = phone.screen;
  document.getElementById("txtBackCamera").value = phone.backCamera;
  document.getElementById("txtFrontCamera").value = phone.fontCamera;
  document.getElementById("txtimg").value = phone.img;
  document.getElementById("txtDesc").value = phone.desc;
  document.getElementById("txtType").value = phone.type;
};

export let layThongTinTuForm = () => {
  let id = document.getElementById("txtIdPhone").value;
  let name = document.getElementById("txtNamePhone").value;
  let price = document.getElementById("txtPrice").value;
  let screen = document.getElementById("txtScreen").value;
  let backCamera = document.getElementById("txtBackCamera").value;
  let fontCamera = document.getElementById("txtFrontCamera").value;
  let img = document.getElementById("txtimg").value;
  let desc = document.getElementById("txtDesc").value;
  let type = document.getElementById("txtType").value;

  return {
    id,
    name,
    price,
    screen,
    backCamera,
    fontCamera,
    img,
    desc,
    type,
  };
};
