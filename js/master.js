let showadmin = () => {
  document.getElementById("admin").style.display = "block";
  document.getElementById("user").style.display = "none";
};
document.getElementById("showadmin").addEventListener("click", showadmin);

let showuser = () => {
  document.getElementById("admin").style.display = "none";
  document.getElementById("user").style.display = "block";
};
document.getElementById("showuser").addEventListener("click", showuser);

export let batLoading = () => {
  document.getElementById("pinner").style.display = "flex";
};
export let tatLoading = () => {
  document.getElementById("pinner").style.display = "none";
};
