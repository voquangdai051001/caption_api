import { batLoading, tatLoading } from "../master.js";
import { renderDSPhone, renderCart } from "./controller.js";

const BAST_URL = "https://6404131480d9c5c7bac044c0.mockapi.io";
let feachDSPhone = () => {
  batLoading();
  axios({
    url: `${BAST_URL}/phone`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      let dsPhone = res.data;
      renderDSPhone(dsPhone);
    })
    .catch((err) => {
      console.log(err);
    });
};
feachDSPhone();

//************ */ thêm hàng vào giỏ********
let addCart = (id) => {
  batLoading();
  axios({
    url: `${BAST_URL}/phone/${id}`,
    method: "GET",
  })
    .then((res) => {
      axios({
        url: `${BAST_URL}/phoneoder`,
        method: "POST",
        data: res.data,
      })
        .then((res) => {
          feachdsPhoneoder();
          showmessage("Thêm thành công vào giỏ hàng");
          console.log("res: ", res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
};
window.addCart = addCart;

// ******************* GIỎ HÀNG**********
// feachdsPhoneoder lấy mới
let feachdsPhoneoder = () => {
  batLoading();
  axios({
    url: `${BAST_URL}/phoneoder`,
    method: `GET`,
  })
    .then((res) => {
      tatLoading();
      let dsPhoneoder = res.data;
      renderCart(dsPhoneoder);
    })
    .catch((err) => {
      console.log(err);
    });
};
feachdsPhoneoder();

// click show cart
let show = () => {
  document.getElementById("listCart").style.display = "block";
};
document.getElementById("show").addEventListener("click", show);

// click close cart
let close = () => {
  document.getElementById("listCart").style.display = "none";
};
document.getElementById("close").addEventListener("click", close);

// nhất delete hàng trong giỏ
let deletedsCart = (id) => {
  batLoading();
  axios({
    url: `${BAST_URL}/phoneoder/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      showmessage("Xóa thành công");
      feachdsPhoneoder();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deletedsCart = deletedsCart;

// thanh toán từng item
let payItemCart = (id) => {
  batLoading();
  axios({
    url: `${BAST_URL}/phoneoder/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      showmessage("Thanh toán thành công");
      feachdsPhoneoder();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.payItemCart = payItemCart;

// toastify THÔNG BÁO
let showmessage = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "left", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
