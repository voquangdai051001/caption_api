export let renderDSPhone = (dsPhone) => {
  let contentHTML = "";
  for (let i = 0; i < dsPhone.length; i++) {
    let contentdiv = `<div class="row align-items-center m-5 pt-4" style="border-radius: 8px; box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 1px 3px 1px;">
        <div class="col-3">
            <img style='width:200px' src="${dsPhone[i].img}" alt="">
            <p> <b> ${dsPhone[i].name} </b> </p>
        </div>
        <div class="col-1">
            <p> <b>Price:</b> ${dsPhone[i].price}</p>           
        </div>
        <div class="col-3">
            <p> <b>Screen:</b> ${dsPhone[i].screen}</p>
            <p> <b>Back Camera:</b> ${dsPhone[i].backCamera}</p>
            <p> <b>Font Camera:</b> ${dsPhone[i].fontCamera}</p>           
        </div>
        <div class="col-3">
            <p> <b>Desc:</b> ${dsPhone[i].desc}</p>
            <p> <b>Type:</b> ${dsPhone[i].type}</p>
        </div>
        <div>
        <button onclick="addCart(${dsPhone[i].id})" class="btn btn-warning">Thêm vào giỏ hàng</button>
        </div>
    </div>`;
    contentHTML += contentdiv;
  }
  document.getElementById("card-body").innerHTML = contentHTML;
};

// show ds hàng trong giỏ
export let renderCart = (dsPhoneoder) => {
  let contentHTML = "";
  for (let i = 0; i < dsPhoneoder.length; i++) {
    let contentdiv = `
      <div class="row m-3 pt-4" style="border-radius: 8px; box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 1px 3px 1px; background-color: blanchedalmond">
      <div class="col-3">
        <img style="width: 100px; display: "block;" src="${dsPhoneoder[i].img}" alt="">
        <p>${dsPhoneoder[i].name}</p>
      </div>
      <div class="col-6">
        <p><b>Screen:</b> ${dsPhoneoder[i].screen}</p>
        <p><b>Back Camera:</b> ${dsPhoneoder[i].backCamera}</p>
        <p><b>Font Camera:</b> ${dsPhoneoder[i].fontCamera}</p>
      </div>
      <div class="col-3">
        <button onclick=(payItemCart(${dsPhoneoder[i].id})) class="btn btn-warning"><b>Price:</b> ${dsPhoneoder[i].price}</button>
        <button onclick=(deletedsCart(${dsPhoneoder[i].id})) class="btn btn-danger mt-3">Delete</button>
      </div>
    </div>`;

    contentHTML += contentdiv;
  }
  document.getElementById("listCart").innerHTML =
    contentHTML +
    `
    `;
};
